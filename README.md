# CURSO EAyO: Introducción al Desarrollo Web

Clases Virtuales: Viernes de 17 a 20hs <br> 

**Temario**

[[_TOC_]]

## Clase 1: HTML
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-web/-/blob/main/Recursos/2021EAyO_Web_Cls_1.pdf) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=o3zgiqfFT2M&feature=youtu.be) <br>

## Clase 2: HTML (parte II)
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-web/-/blob/main/Recursos/2021EAyO_Web_Cls_2.pdf) <br>
- [Recursos](https://gitlab.com/ciorozco/2021-eayo-web/-/blob/main/Recursos/SitioEAyO.zip) <br>
- [Video - Clase grabada](https://youtu.be/piYohvYpPGM) <br>

## Clase 3: HTML (parte III)
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-web/-/blob/main/Recursos/2021EAyO_Web_Cls_3.pdf) <br>
- [Video - Clase grabada](https://youtu.be/GolgxVKIkrQ) <br>

## Clase 4: CSS
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-web/-/blob/main/Recursos/2021EAyO_Web_Cls_4.pdf) <br>
- [Video - Clase grabada](https://youtu.be/d4HiFOcC6qU) <br>

## Clase 5: Bootstrap
- [Diapositiva](https://gitlab.com/ciorozco/2021-eayo-web/-/blob/main/Recursos/2021EAyO_Web_Cls_5.pdf) <br>
- [Video - Clase grabada](https://youtu.be/KKgASm5_B4U) <br>

## Clase 6: Repaso General
- [Sitio Web base](https://gitlab.com/ciorozco/2021-eayo-web/-/blob/main/Recursos/EAyO_sitio.zip) <br>
- [Video - Clase grabada](https://www.youtube.com/watch?v=g-90FqvDe-U) <br>

## Clase 7: Introducción al TP Final

1. Seleccionar un Caso de Estudio particular. Describa las actividades principales que se van a tener en cuenta del mismo.

2. Implementar un sitio Web en Wordpress para el item anterior (Debe contener por lo minimo: 3 páginas y 1 menú)

## Clase 8: Wordpress reportes
- [Video - Clase grabada](https://youtu.be/KetjzNN6qn8) <br>

